//
//  VideoListVC.h
//  My Youtube Channel
//
//  Created by Dulal Hossain on 5/22/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoListCell.h"
#import "YTVideo.h"
#import "YTVideoFetcher.h"
#import "JSONHTTPClient.h"
#import "UIImageView+WebCache.h"
#import "PlayerVC.h"
#import "AlertManager.h"
#import "CoreDBManager.h"
#import "SVProgressHUD.h"

@interface VideoListVC : UIViewController

@property(nonatomic,strong)NSMutableArray *tableData;
@property(nonatomic,strong)IBOutlet UITableView *tableView;
@property(nonatomic,strong)IBOutlet UISegmentedControl *segmentControl;
- (IBAction)tapSegmentControl:(id)sender;

@end

