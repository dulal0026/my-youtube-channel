//
//  PlayerVC.h
//  My Youtube Channel
//
//  Created by Dulal Hossain on 5/26/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+Resize.h"
#import "YTPlayerView.h"
#import "CoreDBManager.h"
#import "YTVideo.h"
#import "AlertManager.h"

@interface PlayerVC : UIViewController<YTPlayerViewDelegate>
@property(nonatomic, strong) IBOutlet YTPlayerView *playerView;
@property(nonatomic, weak) IBOutlet UIButton *playButton;
@property(nonatomic, weak) IBOutlet UIButton *pauseButton;
@property(nonatomic, weak) IBOutlet UIButton *stopButton;
@property(nonatomic, weak) IBOutlet UIButton *startButton;
@property(nonatomic, weak) IBOutlet UIButton *reverseButton;
@property(nonatomic, weak) IBOutlet UIButton *forwardButton;
@property(nonatomic, weak) IBOutlet UITextView *statusTextView;
@property(nonatomic,strong)YTVideo *video;
@property(nonatomic, strong) NSString *videoId;
@property(nonatomic, weak) IBOutlet UISlider *slider;

- (IBAction)onSliderChange:(id)sender;

- (IBAction)buttonPressed:(id)sender;
- (IBAction)shareButtonPressed:(id)sender;
- (IBAction)favoriteButtonPressed:(id)sender;

@end
