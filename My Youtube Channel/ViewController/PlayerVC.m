//
//  PlayerVC.m
//  My Youtube Channel
//
//  Created by Dulal Hossain on 5/26/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import "PlayerVC.h"
static const CGSize kKnobSize = (CGSize){.width = 20, .height = 20};

@interface PlayerVC ()

@end

@implementation PlayerVC


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"Player View";
    NSString *videoId = self.video.songId;
    
    [self.slider setMinimumTrackImage:[[UIImage imageNamed:@"slider_progress_min"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 3, 0, 0)] forState:UIControlStateNormal];
    [self.slider setMaximumTrackImage:[[UIImage imageNamed:@"slider_progress_max"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 3)] forState:UIControlStateNormal];
    
    UIImage* knobImage=[[UIImage imageNamed:@"button_progress_drag"] scaleToSize:kKnobSize];
    [self.slider setThumbImage:knobImage forState:UIControlStateNormal];
    [self.slider setThumbImage:knobImage forState:UIControlStateSelected];
    [self.slider setThumbImage:knobImage forState:UIControlStateHighlighted];
    
    // For a full list of player parameters, see the documentation for the HTML5 player
    // at: https://developers.google.com/youtube/player_parameters?playerVersion=HTML5
    NSDictionary *playerVars = @{
                                 @"controls" : @0,
                                 @"playsinline" : @1,
                                 @"autohide" : @1,
                                 @"showinfo" : @0,
                                 @"modestbranding" : @1
                                 };
    self.playerView.delegate = self;
    [self.playerView loadWithVideoId:videoId playerVars:playerVars];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedPlaybackStartedNotification:)
                                                 name:@"Playback started"
                                               object:nil];
    

    
    
    UIToolbar *tools=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 120, 30)];

    tools.backgroundColor=[UIColor whiteColor];
        tools.barTintColor = self.navigationController.navigationBar.barTintColor;
    [tools setTranslucent:YES];
   // UIBarButtonItem *optionBtn=[[UIBarButtonItem alloc]initWithTitle:@"Share" style:UIBarButtonItemStyleBordered target:self action:@selector(shareButtonPressed:)];
    
    
    UIBarButtonItem *optionBtn=[[UIBarButtonItem alloc] initWithTitle:@"Share" style:UIBarButtonItemStylePlain target:self action:@selector(shareButtonPressed:)];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc] initWithTitle:@"Favorite" style:UIBarButtonItemStylePlain target:self action:@selector(favoriteButtonPressed:)];
    
    NSArray *buttons=[NSArray arrayWithObjects:optionBtn,doneBtn, nil];
    
    [tools setItems:buttons animated:NO];
   
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:tools];
}

- (void)playerView:(YTPlayerView *)ytPlayerView didChangeToState:(YTPlayerState)state {
    NSString *message = [NSString stringWithFormat:@"Player state changed: %ld\n", (long)state];
    [self appendStatusText:message];
}

- (void)playerView:(YTPlayerView *)playerView didPlayTime:(float)playTime {
    float progress = playTime/self.playerView.duration;
    [self.slider setValue:progress];
}

- (IBAction)onSliderChange:(id)sender {
    float seekToTime = self.playerView.duration * self.slider.value;
    [self.playerView seekToSeconds:seekToTime allowSeekAhead:YES];
    [self appendStatusText:[NSString stringWithFormat:@"Seeking to time: %.0f seconds\n", seekToTime]];
}

- (IBAction)buttonPressed:(id)sender {
    if (sender == self.playButton) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Playback started" object:self];
        [self.playerView playVideo];
    } else if (sender == self.stopButton) {
        [self.playerView stopVideo];
    } else if (sender == self.pauseButton) {
        [self.playerView pauseVideo];
    } else if (sender == self.reverseButton) {
        float seekToTime = self.playerView.currentTime - 30.0;
        [self.playerView seekToSeconds:seekToTime allowSeekAhead:YES];
        [self appendStatusText:[NSString stringWithFormat:@"Seeking to time: %.0f seconds\n", seekToTime]];
    } else if (sender == self.forwardButton) {
        float seekToTime = self.playerView.currentTime + 30.0;
        [self.playerView seekToSeconds:seekToTime allowSeekAhead:YES];
        [self appendStatusText:[NSString stringWithFormat:@"Seeking to time: %.0f seconds\n", seekToTime]];
    } else if (sender == self.startButton) {
        [self.playerView seekToSeconds:0 allowSeekAhead:YES];
        [self appendStatusText:@"Seeking to beginning\n"];
    }
}

- (void)receivedPlaybackStartedNotification:(NSNotification *) notification {
    if([notification.name isEqual:@"Playback started"] && notification.object != self) {
        [self.playerView pauseVideo];
    }
}

/**
 * Private helper method to add player status in statusTextView and scroll view automatically.
 *
 * @param status a string describing current player state
 */
- (void)appendStatusText:(NSString *)status {
    [self.statusTextView setText:[self.statusTextView.text stringByAppendingString:status]];
    NSRange range = NSMakeRange(self.statusTextView.text.length - 1, 1);
    // To avoid dizzying scrolling on appending latest status.
    self.statusTextView.scrollEnabled = NO;
    [self.statusTextView scrollRangeToVisible:range];
    self.statusTextView.scrollEnabled = YES;
}
- (IBAction)shareButtonPressed:(id)sender{
    
    [self shareText:self.video.songTitle andImage:nil andUrl:[NSURL URLWithString:self.video.imageUrl]];
}

- (void)shareText:(NSString *)text andImage:(UIImage *)image andUrl:(NSURL *)url
{
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text) {
        [sharingItems addObject:text];
    }
    if (image) {
        [sharingItems addObject:image];
    }
    if (url) {
        [sharingItems addObject:url];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    activityController.excludedActivityTypes= @[UIActivityTypeAssignToContact, UIActivityTypePrint];
    [self presentViewController:activityController animated:YES completion:nil];
}
- (IBAction)favoriteButtonPressed:(id)sender{
    if ([[CoreDBManager sharedInstance] saveVideo:self.video]) [AlertManager showAlertSingle:@"Video successfull saved." msg:@""];
    else{
     [AlertManager showAlertSingle:@"Video not saved." msg:@""];   
    }
}
@end

