//
// VideoListVC.m
//  My Youtube Channel
//
//  Created by Dulal Hossain on 5/22/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#define channelId @"UCFYIoCpXxqM54Wa6222kIIg"
#import "VideoListVC.h"

@interface VideoListVC ()

@end

@implementation VideoListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"My Youtube Channel";
    [self.tableView registerNib:[UINib nibWithNibName:@"VideoListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"VideoListCell"];
    self.segmentControl.selectedSegmentIndex=0;
    [self searchYoutubeVideosForTerm:50];
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)searchYoutubeVideosForTerm:(int)count{
    [SVProgressHUD showWithStatus:@"Please wait..."];
  //  NSString* searchCall = [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?key=AIzaSyDHXGtv0du0VJJZekEYiIMKldy56e2b008&channelId=UCANLZYMidaCbLQFWXBC95Jg&part=snippet,id&order=date&maxResults=50"];
    
     NSString* searchCall = [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?key=AIzaSyDHXGtv0du0VJJZekEYiIMKldy56e2b008&channelId=%@&part=snippet,id&order=date&maxResults=%d",channelId,count];
    
    
    [JSONHTTPClient getJSONFromURLWithString: searchCall
                                  completion:^(NSDictionary *json, JSONModelError *err) {
                                      if (err) {
                                        
                                         [SVProgressHUD showErrorWithStatus:err.localizedDescription];
                                          return;
                                      }
                                      [SVProgressHUD dismiss];
                                      self.tableData=[[[YTVideoFetcher alloc] init] fetchedData:json];
                                    [self.tableView reloadData];
                                  }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tapSegmentControl:(id)sender {
    switch (self.segmentControl.selectedSegmentIndex) {
        case 0:
            [self searchYoutubeVideosForTerm:50];
            break;
        case 1:{
            self.tableData=[[[[CoreDBManager sharedInstance] getAllVideos] valueForKey:@"createVideoInfo"] mutableCopy];
            [self.tableView reloadData];
            break;
        }
        default:
            break;
    }
}

#pragma mark - TableView Delegate/DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tableData? [self.tableData count] : 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VideoListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VideoListCell"];

    YTVideo *video=(YTVideo*)[self.tableData objectAtIndex:indexPath.row];
    
    cell.titleLabel.text = [NSString stringWithFormat:@"%@",video.songTitle];
    NSString *imgURL =[NSString stringWithFormat:@"%@",video.imageUrl];
    NSString* encodedImageUrl = [imgURL stringByAddingPercentEscapesUsingEncoding:
                                 NSUTF8StringEncoding];
    
    [cell.coverImageView sd_setImageWithURL:[NSURL URLWithString:encodedImageUrl]
                             placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    cell.authorNameLabel.text=video.author;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    YTVideo *video=(YTVideo*)[self.tableData objectAtIndex:indexPath.row];
    if (video.songId!=nil) {
     [self performSegueWithIdentifier:@"PlayerVCSegue" sender:indexPath];
    }
    else{
       [AlertManager showAlertSingle:@"Error" msg:@"no id"];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"PlayerVCSegue"]) {
        NSIndexPath *selectedIndexPath = sender;
        //LoanInfo *loanInfo = self.loanArray[selectedIndexPath.row];
        PlayerVC *vc = (PlayerVC *)segue.destinationViewController;
        YTVideo *video=(YTVideo*)[self.tableData objectAtIndex:selectedIndexPath.row];
        
        vc.video =video;
    }
}
@end
