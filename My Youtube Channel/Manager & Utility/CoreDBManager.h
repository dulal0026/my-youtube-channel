//
//  CoreDBManager.h
//  Shopping List
//
//  Created by Dulal Hossain on 11/28/14.
//  Copyright (c) 2014 Rafay Hasan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "YTVideo.h"
#import "Video.h"


@interface CoreDBManager : NSObject
+ (CoreDBManager*) sharedInstance;
@property(nonatomic,strong)NSManagedObjectContext *manageObjectContext;
-(NSManagedObjectContext*)getmanageObject;
-(BOOL)saveVideo:(YTVideo*)videoInfo;
-(void)addVideos:(NSArray*)videoArray;
-(NSMutableArray*)getAllVideos;
-(NSArray*)getVideoArray:(NSString *)objectID;
-(BOOL)isExistVideo:(NSString*)songId;
-(BOOL)removeVideo:(YTVideo*)video;
@end
