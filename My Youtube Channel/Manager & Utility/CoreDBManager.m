//
//  CoreDBManager.m
//  Shopping List
//
//  Created by Dulal Hossain on 11/28/14.
//  Copyright (c) 2014 Rafay Hasan. All rights reserved.
//

#import "CoreDBManager.h"

@implementation CoreDBManager
static CoreDBManager *sharedDataInstance = nil;

-(NSManagedObjectContext*)getmanageObject{
    AppDelegate *app=[[UIApplication sharedApplication] delegate];
    return app.managedObjectContext;
}
+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if (sharedDataInstance == nil){
            sharedDataInstance = [super allocWithZone:zone];
            return sharedDataInstance;
        }
    }
    return nil;
}
-init{
    if (self = [super init]){
    }
    return self;
}
+ (CoreDBManager*) sharedInstance
{
    @synchronized(self){
        if (sharedDataInstance == nil) sharedDataInstance =  [[self alloc] init];
    }
    return sharedDataInstance;
}
#pragma mark - Video List

-(void)addVideos:(NSArray*)videoArray{
    [self fetchAndRemoveAllVideo];
    for (YTVideo *video in videoArray){
        [self saveVideo:video];
    }
}
-(void)fetchAndRemoveAllVideo{
    self.manageObjectContext=[self getmanageObject];
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Video"
                inManagedObjectContext:self.manageObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSError *error;
    NSArray *matching_objects = [self.manageObjectContext executeFetchRequest:request error:&error];
    if ([matching_objects count]>0) {
        for (NSManagedObject *mngObj in matching_objects) {
            [self.manageObjectContext deleteObject:mngObj];
        }
        NSError *saveError = nil;
        [self.manageObjectContext save:&saveError];
    }
}
-(BOOL)removeVideo:(YTVideo*)video{
    BOOL success=NO;
    NSManagedObjectContext *context=[self getmanageObject];
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"Video"];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"songId==%@",video.songId];

    fetchRequest.predicate=predicate;
    
    NSError *error;
    NSArray *objectsArray = [context executeFetchRequest:fetchRequest error:&error];
    if (objectsArray.count!=0) {
        [context deleteObject:[objectsArray objectAtIndex:0]];
    }
    [context save:&error];
    if (error) success=NO;
    else success=YES;
    return success;
}
-(BOOL)saveVideo:(YTVideo*)videoInfo{
    if ([self isExistVideo:videoInfo.songId]) [self removeVideo:videoInfo];
    BOOL success=NO;
    NSManagedObjectContext *context=[self getmanageObject];
    Video *newData = [NSEntityDescription insertNewObjectForEntityForName:@"Video" inManagedObjectContext:context];
    
    newData.songId=videoInfo.songId;
    newData.songTitle=videoInfo.songTitle;
    newData.imageUrl =videoInfo.imageUrl;
    newData.author =videoInfo.author;
   
    NSError *error;
    if ([self.manageObjectContext save:&error]) {
        if (error) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        else{
           success=YES;
        }
    }
        
    return success;
}

-(NSMutableArray*)getAllVideos{
    self.manageObjectContext=[self getmanageObject];
    NSMutableArray *tempArray=[[NSMutableArray alloc] init];
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Video"
                inManagedObjectContext:self.manageObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSError *error;
    NSArray *matching_objects = [self.manageObjectContext executeFetchRequest:request error:&error];
    for (NSManagedObject *info in matching_objects){
        [tempArray addObject:info];
    }
    return tempArray;
}

-(NSArray*)getVideoArray:(NSString *)objectID{
    self.manageObjectContext=[self getmanageObject];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Video"
                inManagedObjectContext:self.manageObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entityDesc];
    NSPredicate *newPredicate =[NSPredicate predicateWithFormat:@"songId == %@",objectID];
    
    [request setPredicate:newPredicate];
    NSError *error;
    NSArray *matching_objects = [self.manageObjectContext executeFetchRequest:request error:&error];
    
    return [NSArray arrayWithArray:matching_objects];
}
-(BOOL)isExistVideo:(NSString*)songId{
    BOOL flag=NO;
    self.manageObjectContext=[self getmanageObject];
    
    NSEntityDescription *entityDesc =
    [NSEntityDescription entityForName:@"Video"
                inManagedObjectContext:self.manageObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    NSPredicate *newPredicate = [NSPredicate predicateWithFormat:@"songId == %@", songId];
    [request setPredicate:newPredicate];
    NSError *error;
    NSArray *matching_objects = [self.manageObjectContext executeFetchRequest:request error:&error];
    if (matching_objects.count!=0) {
        flag=YES;
    }
    return flag;
}
@end
