//
//  Alert Manager
//
//  Created by Dulal Hossain on 03/11/2014.
//  Copyright (c) 2013 Aviel Gross. All rights reserved.
//

#import "AlertManager.h"

@implementation AlertManager

+(void)showAlertSingle:(NSString *)title msg:(NSString *)msg
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
	[alert show];
}

@end
