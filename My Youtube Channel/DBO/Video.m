//
//  Video.m
//  My Youtube Channel
//
//  Created by Dulal Hossain on 6/2/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import "Video.h"


@implementation Video

@dynamic songId;
@dynamic songTitle;
@dynamic author;
@dynamic imageUrl;

@end
