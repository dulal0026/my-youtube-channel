//
//  YTVideoFetcher.h
//  MusicLove
//
//  Created by AAPBD Mac mini on 05/11/2014.
//  Copyright (c) 2014 Dulal Hossain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YTVideo.h"
#import "JSONHTTPClient.h"

@interface YTVideoFetcher : NSObject
- (NSMutableArray*)fetchedData:(NSDictionary *)json;
-(NSString*)fetchedDataShortUrl:(NSDictionary*)dict key:(NSString*)key;
-(NSDictionary*)parseDictionary:(NSString*)url;
-(NSString*)getViedoIdFromUrl:(NSString*)fullUrl;
-(NSString*)fetchVideoId:(NSString*)shortKeyword;
@end
