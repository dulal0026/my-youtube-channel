//
//  YTVideoFetcher.m
//  MusicLove
//
//  Created by AAPBD Mac mini on 05/11/2014.
//  Copyright (c) 2014 Dulal Hossain. All rights reserved.
//

#import "YTVideoFetcher.h"

@implementation YTVideoFetcher
- (NSMutableArray*)fetchedData:(NSDictionary *)json
{
    NSArray *items = [json objectForKey:@"items"];
    
    NSMutableArray *videos=[[NSMutableArray alloc] init];
    for (NSDictionary *item in items)
    {
        YTVideo *ytVideo=[[YTVideo alloc] init];
        
        NSDictionary *idDict = [item objectForKey:@"id"];
        ytVideo.songId=[idDict objectForKey:@"videoId"];
        
        if ([idDict objectForKey:@"videoId"]==nil) ytVideo.songId=[idDict objectForKey:@"playlistId"];
        
        ytVideo.songTitle=[[item objectForKey:@"snippet"] objectForKey:@"title"];
        /*
         NSArray *authors = [item objectForKey:@"author"];
         for (NSDictionary *author in authors){
         ytVideo.author=[[author objectForKey:@"name"] objectForKey:@"$t"];
         }
         ytVideo.duration= [[[[item objectForKey:@"media$group"] objectForKey:@"yt$duration"] objectForKey:@"seconds"] intValue];
         */
        ytVideo.imageUrl=[[[[item objectForKey:@"snippet"] objectForKey:@"thumbnails"] objectForKey:@"default"] objectForKey:@"url"];
        [videos addObject:ytVideo];
    }
    return videos;
}

-(NSString*)fetchedDataShortUrl:(NSDictionary*)dict key:(NSString*)key{
    NSString* value = [dict objectForKey:key];
    return value;
}
-(NSDictionary*)parseDictionary:(NSString*)url{
    NSError *error = nil;
    NSHTTPURLResponse *response = nil;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];

    NSDictionary *json;
    if (data!=nil) {
        NSError *e;
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&e];
        //json=[JSONHTTPClient getJSONFromURLWithString:url];
    }
    return json;
}
-(NSString*)getViedoIdFromUrl:(NSString*)fullUrl{
    NSArray *array=[fullUrl componentsSeparatedByString:@"watch?v="];
    return [NSString stringWithFormat:@"%@",[array objectAtIndex:1]];
}

-(NSString*)fetchVideoId:(NSString*)shortKeyword{
    NSString *videoId=@"";
    if (shortKeyword==nil || [shortKeyword isEqualToString:@""]) {
        return videoId;
    }
    NSString* urlString = [NSString stringWithFormat:@"http://musiclove.fm/yourls/yourls-api.php?signature=05e2685fc7&action=expand&format=json&shorturl=%@",shortKeyword];
  
    NSDictionary *videoDict=[self parseDictionary:urlString];
    NSString *str;
    if (videoDict!=nil){
        str=[self fetchedDataShortUrl:videoDict key:@"longurl"];
    }
    if (str!=nil) {
        videoId=[self getViedoIdFromUrl:str];
    }
    return videoId;
}

@end
