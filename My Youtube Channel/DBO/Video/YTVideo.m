//
//  YTVideo.m
//  MusicLove
//
//  Created by AAPBD Mac mini on 05/11/2014.
//  Copyright (c) 2014 Dulal Hossain. All rights reserved.
//

#import "YTVideo.h"

@implementation YTVideo

@end
@implementation Video (YTVideo)

-(YTVideo*)createVideoInfo{
    YTVideo *videoInfo=[YTVideo new];
    videoInfo.songId=self.songId;
    videoInfo.songTitle=self.songTitle;
    videoInfo.author=self.author;
    videoInfo.imageUrl=self.imageUrl;
    return videoInfo;
}
@end