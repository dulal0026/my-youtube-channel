//
//  YTVideo.h
//  MusicLove
//
//  Created by AAPBD Mac mini on 05/11/2014.
//  Copyright (c) 2014 Dulal Hossain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Video.h"
@interface YTVideo : NSObject
@property (nonatomic, retain)NSString *songId;
@property (nonatomic, retain) NSString * author;
@property (nonatomic) int duration;
@property (nonatomic, retain) NSString * songTitle;
@property (nonatomic, retain)NSString *imageUrl;

@end
@interface Video (YTVideo)
-(YTVideo*)createVideoInfo;
@end