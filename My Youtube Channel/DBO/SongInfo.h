//
//  SongInfo.h
//  My Youtube Channel
//
//  Created by Dulal Hossain on 5/22/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SongInfo : NSObject
@property (nonatomic, retain)NSString *songId;
@property (nonatomic, retain) NSString * author;
@property (nonatomic, retain) NSNumber * duration;
@property (nonatomic, retain) NSString * songTitle;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain)NSString *imageUrl;
@end
