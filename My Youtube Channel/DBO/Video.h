//
//  Video.h
//  My Youtube Channel
//
//  Created by Dulal Hossain on 6/2/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Video : NSManagedObject

@property (nonatomic, retain) NSString * songId;
@property (nonatomic, retain) NSString * songTitle;
@property (nonatomic, retain) NSString * author;
@property (nonatomic, retain) NSString * imageUrl;

@end
