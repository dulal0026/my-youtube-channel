//
//  UIImage+Resize.h
//  Seslenen Kitap
//
//  Created by Dulal Hossain on 15/12/2013.
//  Copyright (c) 2013 Magnifo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Resize)
- (UIImage*)scaleToSize:(CGSize)size;
@end

