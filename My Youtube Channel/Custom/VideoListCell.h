//
//  VideoListCell.h
//  My Youtube Channel
//
//  Created by Dulal Hossain on 5/22/15.
//  Copyright (c) 2015 Dulal Hossain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoListCell : UITableViewCell
@property(nonatomic, strong) IBOutlet UILabel *titleLabel;
@property(nonatomic, strong) IBOutlet UILabel *authorNameLabel;
@property(nonatomic, strong) IBOutlet UIImageView *coverImageView;
@property(nonatomic, strong) IBOutlet UIActivityIndicatorView *indicator;

@end

